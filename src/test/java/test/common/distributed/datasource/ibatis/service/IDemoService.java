/**
 * 
 */
package test.common.distributed.datasource.ibatis.service;

import java.util.Map;

import test.common.distributed.datasource.ibatis.model.DemoModel;
import test.common.distributed.datasource.ibatis.model.DemoParam;

/**
 * @author liubing1
 *
 */
public interface IDemoService {
	
	/**
	 * 插入
	 * @param demoModel
	 */
	public void doCreate(DemoParam demoParam,boolean flag) throws Exception;
	/**
	 * 按照主键删除
	 * @param i
	 */
	public void doDelete(Map<String, Object> params) throws Exception;
	/**
	 * 修改
	 * @param demoModel
	 */
	public void doUpdate(DemoParam demoParam) throws Exception;
	/**
	 * 按照主键查询
	 * @param id
	 * @return
	 */
	public DemoModel findById(Map<String, Object> params) throws Exception;
}
