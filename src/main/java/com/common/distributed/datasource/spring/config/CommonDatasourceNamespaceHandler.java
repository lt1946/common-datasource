/**
 * 
 */
package com.common.distributed.datasource.spring.config;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;
/**
 * @author liubing1
 * 分布式数据源解析
 */
public class CommonDatasourceNamespaceHandler extends NamespaceHandlerSupport {

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.xml.NamespaceHandler#init()
	 */
	@Override
	public void init() {
		// TODO Auto-generated method stub
		registerBeanDefinitionParser("tablename", new TableSchemaBeanParser());
		registerBeanDefinitionParser("database", new DataBaseSchemaBeanParser());
		registerBeanDefinitionParser("datasource", new CommonDataSourceBeanParser());
	}

}
