/**
 * 
 */
package com.common.distributed.datasource.spring.datasource;

import com.common.distributed.datasource.spring.config.schema.DataBaseSchema;


/**
 * @author liubing1
 * spring datasource上下文
 */
public class CustomerContextHolder {
	
	private static final ThreadLocal<DataBaseSchema> contextHolder = new ThreadLocal<DataBaseSchema>();

	/**
	 * 设置数据源
	 * 
	 * @param dataBaseSchema
	 */
	public static void setCustomerType(DataBaseSchema dataBaseSchema) {
		contextHolder.set(dataBaseSchema);
	}

	/**
	 * 获取
	 * 
	 * @return
	 */
	public static DataBaseSchema getCustomerType() {
		return (DataBaseSchema) contextHolder.get();
	}

	/**
	 * 删除
	 */
	public static void clearCustomerType() {
		if(contextHolder.get()!=null){
			contextHolder.remove();
		}
		
	}
}
